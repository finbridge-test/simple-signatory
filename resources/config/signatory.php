<?php
/**
 * Created by PhpStorm.
 * User: imshodan
 * Date: 18.04.20
 * Time: 16:36
 */

return [
    # Секретный ключ, используемый при вычислении цифровой подписи
    'secret_key'  => env('SIGNATORY_SECRET', 'secret'),

    # Название заголовка, в котором содержится значение цифровой подписи данных запроса
    'header_name' => env('SIGNATORY_HEADER_NAME', 'X-SIGN')
];
