<?php
/**
 * Created by PhpStorm.
 * User: imshodan
 * Date: 18.04.20
 * Time: 18:19
 */

namespace Signatory\Interfaces;

/**
 * Interface SignatoryInterface.
 *
 * Интерфейс для сервисов генерации цифровой подписи данных.
 *
 * @package Signatory\Interfaces
 */
interface SignatoryInterface {

    /**
     * Генерирует цифровую подпись данных.
     * @param array $data Данные для подписи
     * @return string
     */
    public function sign(array $data): string;

    /**
     * Выполняет проверку на соответствие данных цифровой подписи
     * @param array $data Проверяемые данные
     * @param string $sign Ключ цифровой подписи
     * @param null|string $newSign
     * @return bool
     */
    public function checkSign(array $data, string $sign, ?string &$newSign = null): bool;
}
