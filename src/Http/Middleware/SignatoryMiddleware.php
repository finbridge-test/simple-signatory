<?php
/**
 * Created by PhpStorm.
 * User: imshodan
 * Date: 18.04.20
 * Time: 23:37
 */

namespace Signatory\Http\Middleware;

use Closure;
use Illuminate\Contracts\Config\Repository as Config;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Signatory\Interfaces\SignatoryInterface;

/**
 * Class SignatoryMiddleware.
 *
 * @package Signatory\Http\Middleware
 */
class SignatoryMiddleware {

    /**
     * @var SignatoryInterface
     */
    protected $signatory;

    /**
     * @var string
     */
    protected $headerName;

    /**
     * SignatoryMiddleware constructor.
     * @param SignatoryInterface $signatory
     * @param Config $config
     */
    public function __construct(SignatoryInterface $signatory, Config $config) {
        $this->signatory  = $signatory;
        $this->headerName = $config->get('signatory.header_name');
    }

    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {
        if (!$request->headers->has($this->headerName)) {
            return new Response('Отсутствует заголовок ' . $this->headerName, 422);
        }

        if ($request->request->count() === 0) {
            return new Response('В запросе отсутствуют данные.', 422);
        }

        $sign = null;
        if (!$this->signatory->checkSign($request->request->all(), $oldSign = $request->header($this->headerName), $sign)) {
            return new Response('Цифровые подписи не совпадают: ' . $oldSign . ' != ' . $sign . '.', 422);
        }

        return $next($request);
    }
}
