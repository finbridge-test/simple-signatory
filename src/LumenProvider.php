<?php
/**
 * Created by PhpStorm.
 * User: imshodan
 * Date: 18.04.20
 * Time: 13:33
 */

namespace Signatory;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Support\ServiceProvider;
use Signatory\Http\Middleware\SignatoryMiddleware;
use Signatory\Interfaces\SignatoryInterface;
use Signatory\Services\SimpleSignatoryService;

/**
 * Class LumenProvider.
 *
 * @package Signatory\Providers
 */
class LumenProvider extends ServiceProvider {

    /**
     * @return void
     */
    public function register(): void {
        $this->mergeConfigFrom(
            __DIR__ . '/../resources/config/signatory.php', 'signatory'
        );

        $this->app->bind(SignatoryInterface::class, function($app) {
            /** @var Repository $config */
            $config = $app['config'];

            return new SimpleSignatoryService(
                $config->get('signatory.secret_key')
            );
        });

        if (method_exists($this->app, 'routeMiddleware')) {
            $this->app->routeMiddleware([
                'signatory' => SignatoryMiddleware::class
            ]);
        }
    }
}
