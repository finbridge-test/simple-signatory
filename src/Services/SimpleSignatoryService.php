<?php
/**
 * Created by PhpStorm.
 * User: imshodan
 * Date: 18.04.20
 * Time: 18:18
 */

namespace Signatory\Services;

use Signatory\Exceptions\EmptyDataException;
use Signatory\Interfaces\SignatoryInterface;

/**
 * Class SimpleSignatoryService.
 *
 * Простой сервис генерации цифровой подписи данных.
 *
 * @package Signatory\Services
 */
class SimpleSignatoryService implements SignatoryInterface {

    /**
     * @var string
     */
    protected $secret;

    /**
     * @var string
     */
    protected $algo;

    /**
     * SimpleSignatoryService constructor.
     * @param string $secret
     * @param string $algo
     */
    public function __construct(string $secret, string $algo = 'sha256') {
        $this->secret = $secret;
        $this->algo   = $algo;
    }


    /**
     * @param object $object
     * @return array
     */
    protected function castToArray($object): array {
        if (method_exists($object, 'toArray')) {
            return $object->toArray();
        }

        return json_decode(json_encode($object), true);
    }

    /**
     * @param array $array
     * @return array
     */
    protected function prepareArray(array $array): array {
        $response = [];
        foreach ($array as $key => $item) {
            if (\is_object($item)) {
                $item = $this->castToArray($item);
            }

            if (\is_array($item)) {
                $item = $this->prepareArray($item);
                ksort($item, SORT_STRING);
            }

            $response[$key] = $item;
        }

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function sign(array $data): string {
        if (\count($data) === 0) {
            throw new EmptyDataException();
        }

        # 1. Преобразовываем каждый элемент в массив + сортируем его
        $preparedArray = $this->prepareArray($data);
        # 2. Сортируем внешний массив
        ksort($preparedArray, SORT_STRING);

        # 3. Преобразовываем в JSON и вычисляем хэш
        return hash_hmac($this->algo, json_encode($preparedArray), $this->secret);
    }

    /**
     * {@inheritdoc}
     */
    public function checkSign(array $data, string $sign, ?string &$newSign = null): bool {
        $newSign = $this->sign($data);

        return $newSign === $sign;
    }
}
