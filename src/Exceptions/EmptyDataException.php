<?php
/**
 * Created by PhpStorm.
 * User: imshodan
 * Date: 18.04.20
 * Time: 19:44
 */

namespace Signatory\Exceptions;

use RuntimeException;
use Throwable;

/**
 * Class EmptyDataException
 * @package Signatory\Exceptions
 */
class EmptyDataException extends RuntimeException {

    /**
     * @var string
     */
    protected const DEFAULT_MESSAGE = 'Невозможно подписать пустой массив.';

    /**
     * EmptyDataException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = '', int $code = 0, Throwable $previous = null) {
        parent::__construct($message ?: static::DEFAULT_MESSAGE, $code, $previous);
    }
}
