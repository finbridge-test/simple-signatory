<?php
/**
 * Created by PhpStorm.
 * User: imshodan
 * Date: 18.04.20
 * Time: 17:51
 */

use PHPUnit\Framework\TestCase;
use Signatory\Exceptions\EmptyDataException;
use Signatory\Services\SimpleSignatoryService;

/**
 * Class SimpleSignatoryServiceTest.
 */
class SimpleSignatoryServiceTest extends TestCase {

    /**
     * @var string
     */
    public const DEFAULT_SECRET = 'secret';

    /**
     * @var SimpleSignatoryService
     */
    protected $service;

    /**
     * @return string
     */
    protected function getSecret(): string {
        return array_key_exists('secret', $_ENV) && $_ENV['secret'] ? $_ENV['secret'] : static::DEFAULT_SECRET;
    }

    /**
     * @return void
     */
    public function setUp(): void {
        $this->service = new SimpleSignatoryService(
            $this->getSecret()
        );
    }

    /**
     * @param array $data
     * @param string $sign
     * @return void
     * @dataProvider oneProvider
     */
    public function testOne(array $data, string $sign): void {
        $this->assertTrue($this->service->checkSign($data, $sign));
    }

    /**
     * @param array $data
     * @param string $sign
     * @return void
     * @dataProvider twoProvider
     */
    public function testTwo(array $data, string $sign): void {
        $this->assertFalse($this->service->checkSign($data, $sign));
    }

    /**
     * @return void
     */
    public function testThree(): void {
        $this->expectException(EmptyDataException::class);
        $this->service->sign([]);
    }

    /**
     * @return array
     */
    public function oneProvider(): array {
        $stdClass     = new stdClass();
        $stdClass->gh = [
            'ef' => [
                0, 5, 4,
                'we' => '/',
                'g'  => new stdClass()
            ]
        ];
        $stdClass->ah = [
            'k' => [
                0, 5, 4,
                'we' => '/',
            ],
            0,
            1   => [
                'a' => 3,
                19
            ]
        ];

        return [
            [
                [
                    'gv' => 14,
                    0,
                    'm'  => 3,
                    1,
                ],
                'dcee1d0a83a588a5ae6a1764bc931cd60b431473adbbc97eaf1aaea13838a63d'
            ],
            [
                [
                    't' => [
                        'tf' => 45
                    ],
                    0,
                    'v' => 56
                ],
                'c26d045fd9cb9210003edaa4b4f5c7aff7a3be07d05e184ffb84fa0a7b642c9c'
            ],
            [
                [
                    's'  => 454,
                    'b'  => 455,
                    'm'  => [
                        'f' => 5770,
                        'b' => new stdClass()
                    ],
                    10   => '4g85',
                    'a1' => $stdClass
                ], '7b57f96c8871440b76e56d8ac8f81c40639cc8034cc3600335776b4ac56e2df4'
            ]
        ];
    }

    /**
     * @return array
     */
    public function twoProvider(): array {
        return [
            [
                [
                    0,
                    'm' => 3,
                    1,
                ],
                'dcee1d0a83a588a5ae6a1764bc931cd60b431473adbbc97eaf1aaea13838a63d'
            ],
            [
                [
                    't' => [
                        'tf' => 45
                    ],
                    'v' => 56
                ],
                'c26d045fd9cb9210003edaa4b4f5c7aff7a3be07d05e184ffb84fa0a7b642c9c'
            ],
            [
                [
                    's' => 454,
                    'b' => 455,
                    'm' => [
                        'f' => 5770,
                        'b' => new stdClass()
                    ],
                    10  => '4g85',
                ], '7b57f96c8871440b76e56d8ac8f81c40639cc8034cc3600335776b4ac56e2df4'
            ]
        ];
    }
}
